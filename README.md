# TIG Stack Edge: Telegraf and Mosquitto

Docker-compose to run Telegraf and Mosquitto. Sends metrics to InfluxDB.

The following additional input plugins are configured in [telegraf.conf](telegraf.conf):

- [[inputs.exec]] to read cpu temperature
- [[inputs.mqtt_consumer]] to get temperature sensor data

## Getting Started

Edit the settings in [.env](.env). For `INFLUXDB_URL` use the server name or IP address, not _localhost_.

Run `docker-compose up` or:

- Start: `make start`
- Stop: `make stop`

## MQTT Test

```
rtl_433 -C si -F json | mosquitto_pub -l -h localhost -t haus/temperatur
```

echo '{"time" : "2021-01-20 19:54:38", "model" : "inFactory-TH", "id" : 78, "channel" : 1, "battery_ok" : 1, "temperature_C" : 20.278, "humidity" : 35, "mic" : "CRC"}' | mosquitto_pub -l -h localhost -t haus/temperatur
